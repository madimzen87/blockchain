REQUESTS:
1)
POST http://{{root}}/api/currencies

POST:
(array) types                       - list of currency types;
(string) start_date[optional]       - start date for time range(YYYY-mm-dd H:i:s);
(string) end_date[optional]         - end date for time range(YYYY-mm-dd H:i:s);

RESPONSE:
Return collection rates for currencies from types array in the range of a given time(start_date - end_date); 

{
    "USD": [
        {
            "last": 9205.17,
            "buy": 9205.17,
            "sell": 9205.17,
            "symbol": "$",
            "date": "2020-07-12 19:20:04"
        }
    ],
    "EUR": [
        {
            "last": 8163.58,
            "buy": 8163.58,
            "sell": 8163.58,
            "symbol": "€",
            "date": "2020-07-12 19:20:04"
        }
    ]
}

2)
GET http://{{root}}/api/currencies/current

RESPONSE:
Return current rate all of currencies


{
    "USD": {
        "15m": 9283.95,
        "last": 9283.95,
        "buy": 9283.95,
        "sell": 9283.95,
        "symbol": "$"
    },
    "AUD": {
        "15m": 13343.01,
        "last": 13343.01,
        "buy": 13343.01,
        "sell": 13343.01,
        "symbol": "$"
    },
    ....
}

