Getting source code from repository

`git clone https://madimzen87@bitbucket.org/madimzen87/blockchain.git` 

Install dependency

`composer install`

Copy .env.example to .env and setup (application name, db connection).

Run:

`php artisan key:generate`

`php artisan migrate`

'php artisan schedule:run'

