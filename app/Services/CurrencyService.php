<?php


namespace App\Services;


use App\Currency;
use App\Http\Requests\Currency\IndexRequest;
use App\Http\Resources\Currency as CurrencyCollection;

class CurrencyService
{
    public function index(IndexRequest $request)
    {

        $types = $request->post('filter');
        $startDate = $request->post('start_date');
        $endDate = $request->post('end_date');
        $collections = [];
        if ($types) {
            foreach ($types as $type) {
                $query = Currency::query()->select(['currencies.last', 'currencies.buy', 'currencies.sell', 'currencies.symbol', 'currencies.created_at AS date'])->where('type', $type)->orderBy('currencies.created_at')->get();

                if ($startDate) {
                    $query = $query->where('date', '>=', $startDate);
                }
                if ($endDate) {
                    $query = $query->where('date', '<=', $endDate);
                }
                $collections[$type] = (new CurrencyCollection($query));
            }
        }
        return $collections;
    }

    public function get()
    {
        $client = new \GuzzleHttp\Client();

        $response = $client->get(
            'https://blockchain.info/ticker',
            [
                'headers' => [
                    'Accept' => 'application/json, */*; q=0.01',
                    'Accept-Encoding' => 'br',
                    'Accept-Language' => 'en;q=0.3',
                    'Connection' => 'keep-alive',
                    'Host' => 'blockchain.info',
                    'TE' => 'Trailers',
                    'Upgrade-Insecure-Requests' => '1',
                    'User-Agent' => 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0',
                ],
                'debug' => false
            ]
        );
        $resultString = $response->getBody()->getContents();
        return json_decode($resultString, true);
    }

    public function import()
    {
        $currencies = $this->get();
        foreach ($currencies as $type => $currency)
        {
            Currency::create([
                'type' => $type,
                'last' => $currency['last'],
                'buy' => $currency['buy'],
                'sell' => $currency['sell'],
                'symbol' => $currency['symbol']
            ]);
        }
        return true;
    }

}
