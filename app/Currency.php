<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Price
 * @package App\Entities
 *
 * @property $id
 * @property $type
 * @property $last
 * @property $buy
 * @property $sell
 * @property $symbol

 */

class Currency extends Model
{

    protected $dates = ['created_at','updated_at'];

    public $fillable = [
        'type',
        'last',
        'buy',
        'sell',
        'symbol',
    ];
}
