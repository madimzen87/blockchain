<?php

namespace App\Http\Controllers;

use App\Currency;
use Illuminate\Http\Request;
use App\Http\Requests\Currency\IndexRequest;
use App\Services\CurrencyService;

class CurrencyController extends Controller
{

    public $currencyService;

    public function __construct(CurrencyService $currencyService)
    {
        $this->currencyService = $currencyService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(IndexRequest $request)
    {
        $data = $this->currencyService->index($request);
        return $data;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function import()
    {
        $currencies = $this->currencyService->import();
        return $currencies;
    }
    public function get()
    {
        $currencies = $this->currencyService->get();
        return $currencies;
    }

}
